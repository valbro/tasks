#include <iostream>

template <int N>
struct Fibo 
{
	enum { value = Fibo<N - 1>::value + Fibo<N - 2>::value };
};

template <>
struct Fibo<1>
{
	enum { value = 1};
};

template <>
struct Fibo<2>
{
	enum { value = 1};
};

int main() {
	int const n = 11;
	int const f = Fibo<n>::value;
	std::cout << n << "th number = " << f << std::endl;
	return 0;
}
