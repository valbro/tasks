#include <iostream>
#include <cmath>

class Vector {
	public:
		Vector(double x) : x(x), N(1) {} 
		Vector(double x, double y) : x(x), y(y), N(2) {}
		Vector(double x, double y, double z) { 
			N = 3; 
			this->x = x;  
			this->y = y;
			this->z = z; 
		}
		double getLength() { 
			switch (N)
			{
				case 1: return x;
				case 2: return sqrt(x * x + y * y); 
				case 3: return sqrt(x * x + y * y + z * z);
			}
		}
	private:
		size_t N;
		double x;
		double y;
		double z;
};
int main () {
	Vector one_d(10), two_d(12, 5), three_d(12, 13, 5);
	std::cout << "1d length = " << one_d.getLength() << std::endl;
	std::cout << "2d length = " << two_d.getLength() << std::endl;
	std::cout << "3d length = " << three_d.getLength() << std::endl;

	return 0;
}
