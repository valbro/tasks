#include <iostream>

class VirtualCell { //и здесь тоже не пойму, а зачем это?
	public:
		virtual double getSize() = 0;
};

class ThreedCell: public VirtualCell {
	public:
		ThreedCell(double x, double y, double z) : x(x), y(y), z(z) {}
		double getSize() { 
			return x * y * z; 
		}
	private:
	double x, y, z;
};

class TwodCell: public VirtualCell {
	public:
		TwodCell(double x, double y) : x(x), y(y) {}
		double getSize() { 
			return x * y; 
		}
	private:
		double x, y;
};

int main() {
	VirtualCell* twodcell = new TwodCell(5, 1);
	VirtualCell* threedcell = new ThreedCell(5, 1, 3);
	std::cout << "S = " << twodcell->getSize() << std::endl;
	std::cout << "V = " << threedcell->getSize() << std::endl;
	return 0;
}
