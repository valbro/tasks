#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <fstream>
#include <set>
#include <ctime>

using std::fstream;
using std::string;
using std::vector;
using std::list;
using std::set;

class FileParser {
	public:
		FileParser(fstream& file) {
			while (file >> tmp) {
				vdata.push_back(tmp);
				ldata.push_back(tmp);
				sdata.insert(tmp);
			}
			std::cout << "vector size " << vdata.size() << std::endl;
			std::cout << "set    size " << sdata.size() << std::endl;
			std::cout << "list   size " << ldata.size() << std::endl;
		}
		vector<string>& getVector() {	return this->vdata;	}
		set<string>& getSet() {	return this->sdata;	}
		list<string>& getList() {	return this->ldata;	}
	private:
		string tmp;
		vector<string> vdata;
		set<string> sdata; 
		list<string> ldata;
		time_t start, end;
};

class StringSearcher {
	public:
		bool Search(vector<string>& vdata, string object) {
			for(size_t i = 0; i < vdata.size(); ++i)
				if(vdata[i] == object) return true;
			return false;
		}

		bool Search(list<string>& ldata, string object) {
			list<string>::iterator it;
			for(it = ldata.begin(); it != ldata.end(); ++it)
				if (*it == object) {
					//std::cout << "iterator = " << *it << "\n";
					return true;
				}
			return false;
		}
		bool Search(set<string>& sdata, string object) {
			if(sdata.find(object) != sdata.end()) return true;
			else return false;
		}
};



int main(int argc, char** argv) {
	fstream file(argv[1], fstream::in);
	FileParser fileparser(file);
	int start, end;
	double dif;
	StringSearcher stringsearcher;
	
	start = clock();
	std::cout << "start vector = " << start << "\n";
	std::cout << "vector " << stringsearcher.Search(fileparser.getVector(), argv[2]) << "\n";
	end = clock();
	std::cout << "end vector = " << end << "\n";
	dif = difftime (end, start);
	printf ("vector took me (%.2lf seconds).\n", dif / CLOCKS_PER_SEC);

	start = clock();
	std::cout << "start set = " << start << "\n";
	std::cout << "set    " << stringsearcher.Search(fileparser.getSet(), argv[2])    << std::endl;
	end = clock();
	std::cout << "end set = " << end << "\n";
	dif = difftime (end,start);
	printf("set took me (%.2lf seconds).\n", dif / CLOCKS_PER_SEC);

	start = clock();
	std::cout << "start list = " << start << "\n";
	std::cout << "list   " << stringsearcher.Search(fileparser.getList(), argv[2])   << std::endl;
	end = clock();
	dif = difftime (end,start);
	std::cout << "end list = " << end << "\n";
	printf ("list took me (%.2lf seconds).\n", dif / CLOCKS_PER_SEC);
	return 0;
}

