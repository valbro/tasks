#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using std::fstream;
using std::string;

class Student {

	public:
		string& r_name() { return this->name; }
		string& r_year() { return this->year; }
		string& r_group() { return this->group; }
		string& r_rock_suxx() { return this->rock_suxx; }
	private:
		string name;
		string year;
		string group;
		string rock_suxx;
};

class StudentFileParser {
	public:
		StudentFileParser(fstream& file) {
			
			size_t i = 0;
			
			while (file >> student.r_name()) 
			{ 
				file >> student.r_year();
				file >> student.r_group();
				file >> student.r_rock_suxx();
				++i;
				all_student.push_back(student);
			}
			N = i;
		}

		std::vector<Student>& GetStudents() { return this->all_student; }
		size_t r_count() { return N; }

	private:
		Student student;
		std::vector<Student> all_student;
		string name;
		size_t N;
};


int main(int argc, char** argv) {
	fstream in(argv[1], fstream::in);
	StudentFileParser studentfileparser(in);
	for(size_t i = 0; i < studentfileparser.r_count(); ++i) {
		std::cout << studentfileparser.GetStudents()[i].r_name() << std::endl;
		std::cout << studentfileparser.GetStudents()[i].r_year() << std::endl;
		std::cout << studentfileparser.GetStudents()[i].r_group() << std::endl;
		std::cout << studentfileparser.GetStudents()[i].r_rock_suxx() << std::endl;
	}
	return 0;
}
