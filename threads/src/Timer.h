class TimerCallback;

class Timer {

TimerCallback* a;

public:
	Timer( TimerCallback* aTimer ) {
		a = aTimer;
	}

	void timerSync( int seconds );
	void timerAsync( int seconds );
};

class TimerCallback {
public:
	virtual void timerExpired( Timer* aTimer ) = 0;
};
