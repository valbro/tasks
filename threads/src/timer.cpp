#include <iostream>
#include <Timer.h>
#include "stdlib.h"
#include <string>
#include <pthread.h>

class Worker:public TimerCallback {
	Timer* aTimer;
public:
	Worker() {
		aTimer = new Timer( this );
	}
	
	void start() {
		aTimer->timerAsync( 1 );
		aTimer->timerAsync( 2 );
		aTimer->timerAsync( 4 );
		aTimer->timerSync( 5 );
		aTimer->timerSync( 1 );
	}

	void timerExpired( Timer* aTimer ) {
		system("ls");
		//std::cout << aTimer << std::endl;
	}

};

void* auxFunc( void* args) {
		system((char*)args);
}

int main() {
	//Worker* w = new Worker();
	//w->start();
	char* s = new char[100];
	void** local;
	size_t N;
	std::cin >> s;
	std::cin >> N;
	system(s);
	local = (void**)s;
	pthread_t* thread = new pthread_t[N];
	int rc = pthread_create( thread, NULL, auxFunc, ( void* ) local );
	return 0;
}
